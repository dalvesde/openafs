#!/bin/bash
AFS_VER=$(sed -n -e '/%define \(afsvers\|pkgrel\) /p' openafs.spec | cut -d' ' -f3 | paste -sd '-' -)
KVER=${2:-latest}
yum -y install cern-yum-tool
cern-yum-tool --prerelease
REPODIR="http://linuxsoft.cern.ch/cern/centos/.8-latest/BaseOS/x86_64/os/Packages"

if [ $1 == "srpm" ]; then
  if [ ${KVER} == "latest" ]; then
    KVER=`yum --enablerepo=cr list kernel | tail -n1 | awk '{print $2}'`
    KERNELDEVEL="kernel-devel-${KVER}"
  else
    KERNELDEVEL="${REPODIR}/kernel-devel-${KVER}.x86_64.rpm"
  fi
  AFS_KVER=`echo ${KVER} | sed 's/-/_/'`
  KMOD_AFS="kmod-openafs-${AFS_VER}.${AFS_KVER}${DIST}.x86_64.rpm"
  curl --silent -o ${WORKDIR}/${KMOD_AFS} http://linuxsoft.cern.ch/internal/repos/openafs8-qa/x86_64/os/Packages/${KMOD_AFS}
  TEST=`file ${WORKDIR}/${KMOD_AFS} | grep -q RPM`
  if [ $? -ne 0 ]; then
    # Store variable for cached files between CI jobs
    echo "export KMOD_AFS_PREEXISTS=false" > ./kmod_afs_exists.env
    yum --enablerepo=cr -y install "$KERNELDEVEL"
    cp -f openafs.spec openafs-kmod-${KVER}.spec
    sed -i "2s/^/%define kernvers ${KVER}\n/" openafs-kmod-${KVER}.spec
    sed -i '0,/%define build_modules/{s/%define build_modules.*/%define build_modules 1\n%define build_userspace 0/}' openafs-kmod-${KVER}.spec
    SANITISED_KVER=`echo $KVER | sed 's/-/_/g'`
    sed -i "s/^%define pkgvers \([0-9.]\+\)/%define pkgvers \1_${SANITISED_KVER}/" openafs-kmod-${KVER}.spec
    rpmbuild -bs --define "dist ${DIST}" --define "_topdir `pwd`/build" --define "_sourcedir `pwd`/src" --define "kernvers ${KVER}" --define "build_userspace 0" --define 'build_modules 1' openafs-kmod-${KVER}.spec
  else
    echo "${KMOD_AFS} exists"
    # Store variable for cached files between CI jobs
    echo "export KMOD_AFS_PREEXISTS=true" > ./kmod_afs_exists.env
  fi
elif [ $1 == "rpm" ]; then
  if [[ $KMOD_AFS_PREEXISTS = "true" ]]; then
    echo "KMOD already exists, cancelling pipeline..."
    # Add sleep so CI job output actually prints echo line
    sleep 5
    # Use API with imageci user to cancel pipeline completely
    curl --request POST --header "PRIVATE-TOKEN: $LINUXCI_APITOKEN" "https://gitlab.cern.ch/api/v4/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/cancel"
  fi

  if [ ${KVER} == "latest" ]; then
    KERNELDEVEL="kernel-devel-${KVER}"
  else
    KERNELDEVEL="${REPODIR}/kernel-devel-${KVER}.x86_64.rpm"
  fi
  rpm -ivh build/SRPMS/*src.rpm
  mv ~/rpmbuild/SPECS/*.spec .
  KVER=`basename -a openafs-kmod*.spec |  sed -e 's/openafs-kmod-//' -e 's/.spec//'`
  yum --enablerepo=cr -y install "$KERNELDEVEL"
  yum-builddep --enablerepo=cr -y openafs-kmod-${KVER}.spec
  rpmbuild -bb --define "dist ${DIST}" --define "_topdir `pwd`/build" --define "_sourcedir `pwd`/src" openafs-kmod-${KVER}.spec
fi
