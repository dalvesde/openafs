#!/bin/bash
# The following script runs CERN CentOS functional tests

yum install git cern-linuxsupport-access openafs-release -y

# Install generated rpm in previous step and copied into VM
yum -y install ~/koji/kmod-openafs*.x86_64.rpm

# Enable access for debugging
cern-linuxsupport-access enable
git clone https://gitlab.cern.ch/linuxsupport/testing/cern_centos_functional_tests.git
cd cern_centos_functional_tests

# Override test skipping. we want to run openafs only which we disable by default
cat > ./skipped-tests.list  <<DELIM
# This file contains list of tests we need/want to skip
# Reason is when there is upstream bug that we're aware of
# So this file should contain:
#  - centos version (using $centos_ver)
#  - test to skip (tests/p_${name}/test.sh)
#  - reason why it's actually skipped (url to upstream BZ, or bug report)
# Separated by |
8|tests/0_common/00_centos_repos.sh|We want to keep CERN repos enabled
8|tests/0_common/30_dns_works.sh|We spawn VM without waiting for DNS register
8|tests/0_common/20_upgrade_all.sh|Avoid too much noise on CI logs
DELIM


# Run afs tests specifically
./runtests.sh cern_openafs
