#!/bin/bash
# The following script installs the kernel version corresponding to the kernel-devel version used for building the kmod-openafs package

# set repos to testing to ensure we get the kernel via testing/cr as soon as possible. We do this as well on build_kmod.sh
yum -y install cern-yum-tool
cern-yum-tool --prerelease

# also configure the repo openafsX-testing/qa to get the latest openafs version
centos_ver=$(rpm -q $(rpm -qf /etc/redhat-release) --queryformat '%{version}\n'|cut -f 1 -d '.')
yum-config-manager --add-repo "http://linuxsoft.cern.ch/internal/repos/openafs${centos_ver}-testing/\$basearch/os"
sed -i "s/\[.*\]/[openafs${centos_ver}-testing]\npriority=1/" /etc/yum.repos.d/linuxsoft.cern.ch*openafs${centos_ver}-testing*.repo
yum-config-manager --add-repo "http://linuxsoft.cern.ch/internal/repos/openafs${centos_ver}-qa/\$basearch/os"
sed -i "s/\[.*\]/[openafs${centos_ver}-qa]\npriority=1/" /etc/yum.repos.d/linuxsoft.cern.ch*openafs${centos_ver}-qa*.repo

# Temporary hack to be able to install older kernels
HACKREPO="http://linuxsoft.cern.ch/internal/kernel-devel8/"
HACKRET=`curl -s -o /dev/null --write-out '%{http_code}\n' $HACKREPO`
if [[ "$HACKRET" -ne 404 ]]; then
  yum-config-manager --add-repo "$HACKREPO"
fi

# Install the kernel version used for the already built kmod package
KVER=$(rpm -qp --queryformat "%{release}\n" ~/koji/kmod-openafs*.rpm | head -n1 | cut -d'.' -f2- | sed 's/_/-/' | sed "s/$DIST//")
INSTALLED_KVER=$(uname -r | sed s/\.`arch`//)

# No need to install if version already matches the one used for building kmod
if [[ "$KVER" == "$INSTALLED_KVER" ]]; then
  echo "Installed kernel already matches the one used to build kmod, no need to reboot"
  # Add sleep so CI job output actually prints echo line
  sleep 5
  exit 0
fi

yum --enablerepo=cr -y install kernel-${KVER}
if [[ $? -ne 0 ]]; then
  echo -e "\e[31mUnable to install kernel-${KVER}\e[0m"
  exit 1
fi
# We need to reboot for this kernel to come in place, as it is the latest, it will automatically selected
sudo reboot

## Warning! Rebooting the node causes ssh to return non-zero exit code, so we have to disable exiting shell with set +e
